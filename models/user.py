from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, Float
from sqlalchemy.orm import relationship

from rdvault.models import EntityBase, ModifiedMixin, CreatedMixin
from rdvault.models.enums import ActivationKeyStatusEnum, UserStatusEnum


class User(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a user
    """
    __tablename__ = 'user'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    name = Column(String, nullable=False)
    email = Column(String, nullable=False)
    plan = Column(String, nullable=False)
    amount = Column(Float, nullable=False)
    status = Column(UserStatusEnum.db_type(), default=UserStatusEnum.active.value)

    # Relational Properties
    account_info = relationship('AccountInfo', backref='user')


class AccountInfo(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Element represents a Account info."
    """
    __tablename__ = 'account_info'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    user_id = Column(Integer, ForeignKey(User.id,
                                         onupdate="cascade",
                                         ondelete="cascade"))
    password = Column(String, nullable=False)


class UserActivationKeys(EntityBase, ModifiedMixin, CreatedMixin):
    """
    Elements represents the User activation keys.
    """
    __tablename__ = 'user_activation_keys'
    __table_args__ = {'schema': 'application'}

    # Scalar properties
    user_id = Column(Integer, ForeignKey(User.id,
                                         onupdate="cascade",
                                         ondelete="cascade"))
    activation_key = Column(String, nullable=False)
    status = Column(ActivationKeyStatusEnum.db_type(), default=ActivationKeyStatusEnum.active.value)
