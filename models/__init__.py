from sqlalchemy import Column
from sqlalchemy import Integer, DateTime
from sqlalchemy.ext.declarative import declarative_base
from rdvault.utils import utc_timestamp

Base = declarative_base()


class EntityBase(Base):
    """
    Element represents the key column for all tables.
    """
    __abstract__ = True

    # Scalar properties
    id = Column(Integer, primary_key=True, autoincrement=True)


class ModifiedMixin(object):
    """
    Element represents the modification related set of columns
    """
    # Scalar properties
    modified = Column(DateTime(timezone=True),
                      default=utc_timestamp,
                      onupdate=utc_timestamp)


class CreatedMixin(object):
    """
    Element represents the creation related set of columns
    """
    # Scalar properties
    created = Column(DateTime(timezone=True),
                      default=utc_timestamp,
                      onupdate=utc_timestamp)
