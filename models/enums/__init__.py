from enum import Enum
from sqlalchemy.types import Enum as EnumType
from rdvault.settings.constants import STATUS_ACTIVE, STATUS_INACTIVE, STATUS_BLACKLIST


class EnumBase(Enum):
    @classmethod
    def db_type(cls):
        return EnumType(*cls.values(), name=cls.__type_name__,
                        schema=cls.__schema__)

    @classmethod
    def values(cls):
        return [item.value for item in cls]

    @classmethod
    def items(cls):
        return [(item.name, item.value) for item in cls]


class UserStatusEnum(EnumBase):
    __type_name__ = 'user_status_enum'
    __schema__ = 'application'
    __order__ = 'active inactive blacklist'

    active = STATUS_ACTIVE
    inactive = STATUS_INACTIVE
    blacklist = STATUS_BLACKLIST


class ActivationKeyStatusEnum(EnumBase):
    __type_name__ = 'activation_key_status_enum'
    __schema__ = 'application'
    __order__ = 'active inactive'

    active = STATUS_ACTIVE
    inactive = STATUS_INACTIVE
