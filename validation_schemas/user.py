from rdvault.settings.constants import NAME_PATTERN
from rdvault.settings.constants import CUSTOMER_NAME, EMAIL, INSURED_AMOUNT, INSURANCE_PLAN

SCHEMA_DEF = {
    EMAIL: {'type': 'string', 'format': 'email'},
    CUSTOMER_NAME: {
        'type': 'string',
        'minLength': 1,
        'pattern': NAME_PATTERN
    },
    INSURANCE_PLAN: {'type': 'string', 'minLength': 1, 'maxLength': 200},
    INSURED_AMOUNT: {'type': 'number', 'minimum': 1, 'maximum': 5000000}
}

CUST_REGISTER_SCHEMA = {
    'title': 'Customer Registerion',
    'type': 'object',
    'required': [EMAIL, CUSTOMER_NAME, INSURANCE_PLAN, INSURED_AMOUNT],
    'additionalProperties': False,
    'properties': {
        CUSTOMER_NAME: SCHEMA_DEF[CUSTOMER_NAME],
        EMAIL: SCHEMA_DEF[EMAIL],
        INSURANCE_PLAN: SCHEMA_DEF[INSURANCE_PLAN],
        INSURED_AMOUNT: SCHEMA_DEF[INSURED_AMOUNT]
    }
}
