from http import HTTPStatus

from rdvault.settings.constants import CUSTOMER_NAME, EMAIL, INSURED_AMOUNT, INSURANCE_PLAN
from rdvault.repository.sql.user import UserRepo, UserActivationKeysRepo
from rdvault.utils import get_md5_hash, utc_timestamp
from rdvault.utils.emailer import Emailer
from rdvault.settings import HOST, PORT, ACT_KEY_VALIDITY


class UserService(object):
    def __init__(self):
        self.user_repo = UserRepo()
        self.act_key_repo = UserActivationKeysRepo()
        self.emailer = Emailer()

    def register_user(self, user_info):
        email = user_info[EMAIL]
        response = {}
        if self.is_blacklisted(email):
            response['message'] = 'User {} is blacklisted'.format(email)
            response['code'] = HTTPStatus.FORBIDDEN
            return response

        if self.is_user_exist(email):
            response['message'] = 'User {} is already registered'.format(email)
            response['code'] = HTTPStatus.BAD_REQUEST
            return response

        try:
            self.user_repo.create(user_info[CUSTOMER_NAME], email, user_info[INSURANCE_PLAN], user_info[INSURED_AMOUNT])
            response['message'] = 'User registration successful. Activation link sent to {}'.format(email)
            response['code'] = HTTPStatus.CREATED
            self.send_activation_link(email)

        except Exception as ex:
            response['message'] = 'Error while creating user. {}'.format(str(ex))
            response['code'] = HTTPStatus.INTERNAL_SERVER_ERROR

        return response

    def activate_user(self, act_key):
        response = {}
        user_act_obj = self.get_user_act_obj(act_key)
        if not user_act_obj:
            response['message'] = 'Incorrect activation code provided.'.format(act_key)
            response['code'] = HTTPStatus.BAD_REQUEST
            return response

        if self.is_act_key_expired(user_act_obj):
            response['message'] = 'Activation key expired. It remains valid only for {} minutes.'.format(ACT_KEY_VALIDITY)
            response['code'] = HTTPStatus.BAD_REQUEST
            return response

        try:
            user = self.user_repo.update_by_id(user_act_obj.user_id)
            self.act_key_repo.update(user_act_obj)
            response['message'] = 'User activation successful. Account details sent to {}'.format(user.email)
            response['code'] = HTTPStatus.OK
            self.send_activation_success_email(user)

        except Exception as ex:
            response['message'] = 'Error while activating user. {}'.format(str(ex))
            response['code'] = HTTPStatus.INTERNAL_SERVER_ERROR

        return response

    def get_user_act_obj(self, act_key):
        return self.act_key_repo.get_by_act_key(act_key)

    def is_blacklisted(self, email):
        return self.user_repo.check_is_blacklisted(email)

    def is_user_exist(self, email):
        return self.user_repo.get_by_email(email)

    def is_act_key_expired(self, act_key_obj):
        now = utc_timestamp()
        created = act_key_obj.created

        return ((now - created).total_seconds() / 60) > ACT_KEY_VALIDITY

    def send_activation_link(self, email):
        activation_code = get_md5_hash(email)
        user = self.user_repo.get_by_email(email)

        self.act_key_repo.create_obj(user.id, activation_code)
        self.send_activation_email(email, activation_code)

    def send_activation_email(self, email, act_key):
        msg = '''
        Congratulations! You have been registered successfully.
        Please click on below link for activating account.
        
        http://{}:{}/user/activate?activation_code={}
        
        This activation code will active only for 15 minutes.
        
        Thank you
        '''
        sub = 'Activation Link'

        msg = msg.format(HOST, PORT, act_key)
        self.emailer.send_email(to=email, subject=sub, message=msg)

    def send_activation_success_email(self, user):
        msg = '''
        Congratulations! Your account has been activated successfully.
        
        Following are your account details:
        Name - {}
        Email - {}
        Insurance Plan - {}
        Insurance Amount - {}

        Thank you
        '''
        sub = 'Registration Successful'

        msg = msg.format(user.name, user.email, user.plan, user.amount)
        self.emailer.send_email(to=user.email, subject=sub, message=msg)

