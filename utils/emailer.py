import smtplib

from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

from rdvault.settings import ACCOUNT_EMAIL, EMAIL_PSWD


class Emailer(object):
    def __init__(self, username=ACCOUNT_EMAIL, password=EMAIL_PSWD):
        self.username = username
        self.password = password

    def send_email(self, to, subject, message):
        msg = MIMEMultipart()
        msg['From'] = self.username
        msg['To'] = to
        msg['Subject'] = subject

        msg.attach(MIMEText(message, 'plain'))

        msg = msg.as_string()
        smtp_server = smtplib.SMTP('smtp.gmail.com', 587)
        smtp_server.starttls()

        smtp_server.login(ACCOUNT_EMAIL, EMAIL_PSWD)
        smtp_server.sendmail(ACCOUNT_EMAIL, to, msg)
        smtp_server.quit()
