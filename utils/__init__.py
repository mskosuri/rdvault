import hashlib
import json
import string
import pytz
import random

from datetime import datetime
from functools import wraps


from flask import Response
from flask import request
from jsonschema import validate, ValidationError
from jsonschema import draft4_format_checker

ERR_INVALID_JSON = 'Invalid JSON: {}'


def password_generator(size=8, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def utc_timestamp():
    return pytz.utc.localize(datetime.utcnow())


def get_md5_hash(text):
    return hashlib.md5(text.encode('utf-8')).hexdigest()


def validate_json_input(schema):
    """
    Decorator to validate the request's JSON input

    Return ``httplib.BAD_REQUEST`` on error.

    It will ensure:
    - JSON input (request.body) validates against the given JSON ``schema``

    """
    def decorator(view_method):
        @wraps(view_method)
        def wrapper(*args, **kwargs):
            error = None
            try:
                validate(request.json, schema,
                         format_checker=draft4_format_checker)
            except ValueError as err:
                error = str(err)
            except ValidationError as err:
                error = ERR_INVALID_JSON.format(err.message)

            if error is not None:
                res = {
                    'status': 'VALIDATION-ERROR',
                    'reason': error
                }
                return Response(json.dumps(res), status=400, mimetype='application/json')
            return view_method(*args, **kwargs)
        return wrapper
    return decorator
