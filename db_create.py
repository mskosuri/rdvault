import random
from rdvault.settings import ADMIN_DB_NAME, ADMIN_DB_HOST, ADMIN_DB_HOST_PORT
from rdvault.settings import ADMIN_DB_USER, ADMIN_DB_PASSWORD, SQL_ENDPOINT_FORMAT

from sqlalchemy.schema import CreateSchema
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine as c_engine
from rdvault.models.user import User, AccountInfo, UserActivationKeys
from rdvault.models.enums import ActivationKeyStatusEnum, UserStatusEnum
from rdvault.models import Base, EntityBase
from sqlalchemy import *


metadata = MetaData()
SAMPLE_USERS = 10
BLACKLISTED_USERS = 5


endpoint = SQL_ENDPOINT_FORMAT.format(ADMIN_DB_USER, ADMIN_DB_PASSWORD,
                                      ADMIN_DB_HOST, ADMIN_DB_HOST_PORT,
                                      ADMIN_DB_NAME)

engine = c_engine(endpoint, echo=True)
engine.execute(CreateSchema('application'))
Base.metadata.create_all(engine)
Session = sessionmaker(bind=engine)
session = Session()

for i in range(BLACKLISTED_USERS):
    user = User()
    user.name = 'Blacklisted_User_{}'.format(i)
    user.status = UserStatusEnum.blacklist.value
    user.email = 'bl_user{}@rdvault.com'.format(i)
    user.plan = 'MyCustomPlan_{}'.format(i)
    user.amount = random.randrange(10000, 100000)
    session.add(user)

session.commit()