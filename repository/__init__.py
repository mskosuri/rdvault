from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from rdvault.settings import SQL_ENDPOINT_FORMAT
from rdvault.settings import ADMIN_DB_NAME, ADMIN_DB_HOST, ADMIN_DB_HOST_PORT
from rdvault.settings import ADMIN_DB_USER, ADMIN_DB_PASSWORD


class SqlContext(object):
    def __init__(self):
        self.session = self.get_session()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.close()

    def close(self):
        if self.session:
            self.session.close()

    @staticmethod
    def get_endpoint(user=ADMIN_DB_USER, password=ADMIN_DB_PASSWORD,
                     host=ADMIN_DB_HOST, port=ADMIN_DB_HOST_PORT, name=ADMIN_DB_NAME):
        return SQL_ENDPOINT_FORMAT.format(user, password, host, port, name)

    @staticmethod
    def get_engine(endpoint):
        engine = create_engine(endpoint)
        return engine

    def get_session(self):
        end_point = self.get_endpoint()
        engine=self.get_engine(end_point)
        Session = sessionmaker(bind=engine)
        return Session()
