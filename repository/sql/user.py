from rdvault.models.user import User, UserStatusEnum, UserActivationKeys, ActivationKeyStatusEnum
from rdvault.repository import SqlContext


class UserRepo(object):
    def __init__(self):
        self.context = SqlContext()
        self.session = self.context.session

    def get_all_query(self):
        query = self.session.query(User)
        return query

    def get_all(self):
        query = self.get_all_query()
        return query.all()

    def get_by_id(self, user_id):
        query = self.get_all_query()
        query = query.filter(User.id == user_id)
        return query.scalar()

    def get_by_email(self, email):
        query = self.get_all_query()
        query = query.filter(User.email.ilike(email))
        return query.scalar()

    def check_is_blacklisted(self, email):
        query = self.get_all_query()
        query = query.filter(User.email.ilike(email),
                             User.status == UserStatusEnum.blacklist.value)
        return query.scalar()

    def create(self, name, email, plan, amount):
        user = User()
        user.name = name
        user.email = email
        user.plan = plan
        user.amount = amount
        user.status = UserStatusEnum.inactive.value

        self.session.add(user)
        self.session.commit()
        self.session.flush()

        return user

    def update_by_id(self, user_id):
        user = self.get_by_id(user_id)
        user.status = UserStatusEnum.active.value
        self.session.add(user)
        self.session.commit()
        self.session.flush()

        return user


class UserActivationKeysRepo(object):
    def __init__(self):
        self.context = SqlContext()
        self.session = self.context.session

    def get_all_query(self):
        query = self.session.query(UserActivationKeys)
        return query

    def get_by_email(self, email):
        query = self.get_all_query()
        query = query(UserActivationKeys).join(User).filter(User.email.ilike(email))
        return query.scalar()

    def get_by_act_key(self, act_key):
        query = self.get_all_query()
        query = query.filter(UserActivationKeys.activation_key.ilike(act_key),
                             UserActivationKeys.status == ActivationKeyStatusEnum.active.value)
        return query.scalar()

    def create_obj(self, user_id, act_key):
        user_act_key = UserActivationKeys()
        user_act_key.user_id = user_id
        user_act_key.activation_key = act_key
        user_act_key.status = ActivationKeyStatusEnum.active.value

        self.session.add(user_act_key)
        self.session.commit()
        self.session.flush()

        return user_act_key

    def update(self, user_act_key):
        user_act_key.status = ActivationKeyStatusEnum.inactive.value
        self.session.add(user_act_key)
        self.session.commit()
        self.session.flush()

        return user_act_key
