import json
from http import HTTPStatus

from flask import Flask
from flask import Response
from flask import request


from rdvault.service.user_service import UserService
from rdvault.validation_schemas.user import CUST_REGISTER_SCHEMA
from rdvault.utils import validate_json_input
from rdvault.settings import HOST, PORT

app = Flask(__name__)
user_service = UserService()


@app.route('/')
def welcome():
    res_msg = "Welcome to RDVault user registration app."
    response = Response(res_msg, status=HTTPStatus.OK, mimetype='application/json')
    return response


@app.route('/register', methods=['POST'])
@validate_json_input(CUST_REGISTER_SCHEMA)
def register_user():
    req_json = request.json
    res = user_service.register_user(req_json)
    response = Response(json.dumps(res['message']), status=res['code'], mimetype='application/json')
    return response


@app.route('/user/activate', methods=['GET'])
def activate_user():
    try:
        act_code = request.args['activation_code']
    except KeyError as ex:
        return Response('Activation code missing in request.', status=HTTPStatus.BAD_REQUEST, mimetype='application/json')

    res = user_service.activate_user(act_code)
    response = Response(json.dumps(res['message']), status=res['code'], mimetype='application/json')
    return response


if __name__ == '__main__':
    app.run(host=HOST, port=PORT)
