# Input Json Fields
CUSTOMER_NAME = 'customer-name'
EMAIL = 'email-address'
INSURANCE_PLAN = 'insurance-plan-name'
INSURED_AMOUNT = 'insured-amount'

# Regex Patterns
NAME_PATTERN = '[a-zA-Z]+\s*[a-zA-Z]*$'

# Status Types
STATUS_ACTIVE = 'ACTIVE'
STATUS_INACTIVE = 'INACTIVE'
STATUS_BLACKLIST = 'BLACKLISTED'

MIME_TYPE_JSON = 'application/json'
