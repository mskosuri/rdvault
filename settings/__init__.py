APP_NAME = 'rdvault'

# DB configurations
SQL_ENDPOINT_FORMAT = 'postgresql://{0}:{1}@{2}:{3}/{4}'
ADMIN_DB_HOST = '127.0.0.1'
ADMIN_DB_HOST_PORT = 5432
ADMIN_DB_NAME = APP_NAME
ADMIN_DB_USER = 'postgres'
ADMIN_DB_PASSWORD = 'postgres'


# EMAIL Credentials
ACCOUNT_EMAIL = 'rdvault.test@gmail.com'
EMAIL_PSWD = 'rdv@ultTest123'


# Server Settings
HOST = 'localhost'
PORT = 5000

ACT_KEY_VALIDITY = 15